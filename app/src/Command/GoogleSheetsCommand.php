<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpClient\HttpClient;

#[AsCommand(
    name: 'app:google-sheets',
    description: 'This app to reading orders from google sheets files',
)]
class GoogleSheetsCommand extends Command
{

    protected $responce;

    public function getResponce(): string
    {
        return $this->responce;
    }
    public function setResponce(string $responce): void
    {
        $this->responce = $responce;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        $io->success('Заказы успешно сохранены.');

        $output->writeln('Файл считан');

        return Command::SUCCESS;
    }


    private function requestGoogleSheets()
    {

        $client = HttpClient::create();

        $response = $client->request('GET', 'https://sheets.googleapis.com/v4/spreadsheets/1nTEQsrDpWSR-6nQHjaGkfmgpuHPVPrahuKWn8nalNMo?includeGridData=true&key=AIzaSyA5T-7WtgZy0Rts7_dIPsso13cDX1-NN1c');

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

        $this->setResponce($response);

    }

    private function saveData()
    {

            //Проверяем ответ запроса

            //Разбираем данные, проверяем структуру файла

            //Сохраняем данные в базу

            //Перемещаем файл в пупку архив на google disk в случае успеха

            //Иначе перемещаем файл в папку invalid

            //Возвращаем true || false

    }

}
